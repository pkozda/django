from django.test import TestCase, Client
from .models import MyUser


class MainPageTest(TestCase):

    def test_main_page_available(self):
        """Testing main page availability."""
        c = Client()
        response = c.get('/')
        self.assertEquals(response.status_code, 200)


class LoginRegister(TestCase):

    def test_login_testing(self):
        """Testing authentication."""
        c = Client()
        MyUser.objects.create_user(email='user@local.host',
                                   username='user',
                                   password='pass')
        c.login(username='pass', password='pass')
