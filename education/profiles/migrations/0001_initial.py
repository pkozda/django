# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='MyUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(verbose_name='superuser status', default=False, help_text='Designates that this user has all permissions without explicitly assigning them.')),
                ('username', models.CharField(validators=[django.core.validators.RegexValidator('^[0-9a-zA-Z]*$', message='Only alphanumeric characters are allowed.')], unique=True, max_length=30)),
                ('email', models.EmailField(verbose_name='email address', unique=True, max_length=255)),
                ('first_name', models.CharField(null=True, blank=True, max_length=50)),
                ('last_name', models.CharField(null=True, blank=True, max_length=50)),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('user_bio', models.TextField()),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(to='auth.Group', related_query_name='user', verbose_name='groups', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', blank=True)),
                ('user_permissions', models.ManyToManyField(to='auth.Permission', related_query_name='user', verbose_name='user permissions', help_text='Specific permissions for this user.', related_name='user_set', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
