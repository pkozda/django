from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponseRedirect
from django.views import generic
from django.core.urlresolvers import reverse
from .forms import UserCreationForm, UserChangeForm
from .models import MyUser


class IndexView(generic.base.TemplateView):
    template_name = 'profiles/index.html'


class RegisterFormView(generic.edit.FormView):
    """Register Class for new users."""

    form_class = UserCreationForm
    success_url = '/'
    template_name = 'profiles/register.html'

    def form_valid(self, form):
        form.save()
        username = self.request.POST['username']
        password = self.request.POST['password1']
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(generic.edit.FormView):
    """Login Class to auth users."""

    form_class = AuthenticationForm
    template_name = 'profiles/login.html'
    success_url = '/todo/'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(generic.base.View):
    """Logout Class to logout users."""

    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class ProfileView(generic.edit.UpdateView):
    """Profile where user can change his info."""

    model = MyUser
    form_class = UserChangeForm
    template_name = 'profiles/profile.html'

    def get_success_url(self):
        return reverse('profiles:profile', kwargs={'pk': self.request.user.id})

    def form_valid(self, form):
        form.instance.password = self.request.user.password
        return super(ProfileView, self).form_valid(form)
