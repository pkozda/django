from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.utils.decorators import wraps
from django.shortcuts import get_object_or_404
from .models import List


class LoginRequiredMixin(object):
    """Ensures that user must be authenticated in order to access view."""

    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)

def admin_or_permissions_required(pk, *permissions):
    """Requires user be admin or have permissions to access."""

    def _decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            user = request.user
            model = List
            grant = False
            if user.is_authenticated():
                list_pk = kwargs.get(pk, None)
                cur_list = get_object_or_404(model, pk=list_pk)
                if user in cur_list.users.filter(listusers__permission__in=permissions) or \
                   user.is_admin:
                    grant = True
            if not grant:
                return HttpResponseRedirect('/todo/wrong')
            return view_func(request, *args, **kwargs)
        return _wrapped_view
    return _decorator
