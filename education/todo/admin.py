from django.contrib import admin
from .models import List, Task


class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'list', 'description', 'time', 'completed')
    list_filter = ('time',)
    search_fields = ('name',)


class TaskInline(admin.TabularInline):
    model = Task


class ListAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_date')
    list_filter = ('created_date',)
    search_fields = ('name',)
    inlines = [TaskInline]


admin.site.register(List, ListAdmin)
admin.site.register(Task, TaskAdmin)
