from django.db import models
from django.core.urlresolvers import reverse


class List(models.Model):
    """Model for creating list of future tasks"""

    name = models.CharField(max_length=100)
    users = models.ManyToManyField('profiles.MyUser', through='ListUsers')
    created_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('todo:detail', kwargs={'pk': self.id})


class ListUsers(models.Model):
    """Middleware model with permissions."""

    PERMISSIONS = (
        ('a', 'Author'),
        ('r', 'Read only'),
        ('e', 'Read and edit'),
    )

    user = models.ForeignKey('profiles.MyUser')
    list = models.ForeignKey('List')
    permission = models.CharField(max_length=1, choices=PERMISSIONS)


class Task(models.Model):
    """Model for creating task of the list"""

    name = models.CharField(max_length=100)
    list = models.ForeignKey('List')
    description = models.TextField(blank=True, null=True)
    time = models.TimeField()
    completed = models.BooleanField(default=None)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('todo:detail', kwargs={'pk': self.list.id})
