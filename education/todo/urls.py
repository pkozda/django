from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^wrong$', views.WrongView.as_view(), name='wrong_way'),

    # urls for add, update, view and delete lists
    url(r'^new$', views.CreateList.as_view(), name='add_list'),
    url(r'^shared$', views.SharedList.as_view(), name='shared_list'),
    url(r'^(?P<pk>[0-9]+)/$',
        views.ListDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/edit/$',
        views.UpdateList.as_view(), name='update_list'),
    url(r'^(?P<pk>[0-9]+)/share/$',
        views.ShareList.as_view(), name='share_list'),
    url(r'^(?P<pk>[0-9]+)/delete/$',
        views.DeleteList.as_view(), name='delete_list'),


    # urls for add, update and delete tasks
    url(r'^(?P<list_pk>[0-9]+)/new$',
        views.CreateTask.as_view(), name='add_task'),
    url(r'^(?P<list_pk>[0-9]+)/(?P<pk>[0-9]+)$',
        views.UpdateTask.as_view(), name='update_task'),
    url(r'^(?P<list_pk>[0-9]+)/(?P<pk>[0-9]+)/delete$',
        views.DeleteTask.as_view(), name='delete_task'),
]
