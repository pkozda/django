from django.test import TestCase, Client
from .models import List, Task


class ListTests(TestCase):
    """List model tests."""

    def test_str(self):
        """Testing str method of class."""
        my_list = List(name='Shopping')
        self.assertEquals(str(my_list), 'Shopping')

    def test_main_page_available(self):
        """Testing main page availability."""
        c = Client()
        response = c.get('/')
        self.assertEquals(response.status_code, 200)


class TaskTests(TestCase):
    """List model tests."""

    def test_str(self):
        """Testing str method of class."""
        my_task = Task(name='Milk')
        self.assertEquals(str(my_task), 'Milk')
