from django.shortcuts import get_object_or_404
from django.views import generic
from django.http import HttpResponseRedirect
from .forms import ManageListForm, AddTaskForm, UpdateTaskForm, ShareListForm
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse
from .models import List, Task, ListUsers
from .decorators import LoginRequiredMixin, admin_or_permissions_required
from profiles.models import MyUser
from django.utils.decorators import method_decorator


class WrongView(generic.TemplateView):
    """Template view for access denied."""

    template_name = 'todo/wrong_way.html'


class IndexView(LoginRequiredMixin, generic.ListView):
    """View for printing all lists from database."""

    model = List
    template_name = 'todo/index.html'

    def get_queryset(self):
        user = self.request.user
        lists = user.list_set.filter(listusers__permission='a')
        return lists


class ListDetailView(LoginRequiredMixin, generic.DetailView):
    """View for printing details about list."""

    model = List
    template_name = 'todo/detail.html'
    cur_list = None
    perm = None

    def get_context_data(self, **kwargs):
        context = super(ListDetailView, self).get_context_data(**kwargs)
        author = self.cur_list.users.get(listusers__permission='a')
        perm = self.perm
        context['author'] = author
        context['perm'] = perm
        return context

    def get(self, request, *args, **kwargs):
        user = self.request.user
        list_pk = kwargs.get('pk', None)
        self.cur_list = get_object_or_404(List, pk=list_pk)
        try:
            self.perm = user.listusers_set.get(list=self.cur_list).permission
        except ListUsers.DoesNotExist:
            return HttpResponseRedirect('/todo/wrong')
        return super(ListDetailView, self).get(request, *args, **kwargs)


class SharedList(LoginRequiredMixin, generic.ListView):
    """View for printing all user's lists."""

    model = List
    template_name = 'todo/shared_list.html'

    def get_queryset(self):
        user = self.request.user
        lists = user.list_set.filter(listusers__permission__in=['r', 'e'])
        return lists


class CreateList(LoginRequiredMixin, generic.CreateView):
    """View for creating new list."""

    form_class = ManageListForm
    template_name = 'todo/add_list.html'

    def form_valid(self, form):
        new_list = form.save()
        user = self.request.user
        ListUsers.objects.create(user=user,
                                 list=new_list,
                                 permission='a')
        return super(CreateList, self).form_valid(form)


class UpdateList(LoginRequiredMixin, generic.UpdateView):
    """View for updating current list."""

    form_class = ManageListForm
    model = List
    template_name = 'todo/update_list.html'

    @method_decorator(admin_or_permissions_required('pk', 'a', 'e'))
    def get(self, request, *args, **kwargs):
        return super(UpdateList, self).get(request, *args, **kwargs)


class ShareList(generic.FormView):
    """Here must be view for sharing list from one user to another."""

    form_class = ShareListForm
    model = List
    template_name = 'todo/share_list.html'
    success_url = '/todo/'
    cur_list = None

    def get_list(self):
        list_pk = self.kwargs.get('pk', None)
        self.cur_list = get_object_or_404(List, pk=list_pk)

    @method_decorator(admin_or_permissions_required('pk', 'a'))
    def get(self, request, *args, **kwargs):
        return super(ShareList, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        name = form.cleaned_data.get('username', None)
        perms = form.cleaned_data.get('permissions', None)
        try:
            user = MyUser.objects.get(username=name)
        except MyUser.DoesNotExist:
            return HttpResponseRedirect('/todo/wrong')
        self.get_list()
        try:
            shared = user.listusers_set.get(list=self.cur_list)
        except ListUsers.DoesNotExist:
            shared = None
        if shared:
            if shared.permission != 'a':
                shared.permission = perms
                shared.save()
        else:
            ListUsers.objects.create(user=user,
                                     list=self.cur_list,
                                     permission=perms)
        return super(ShareList, self).form_valid(form)


class DeleteList(LoginRequiredMixin, generic.DeleteView):
    """View for deleting current list."""

    model = List
    template_name = 'todo/list_confirm_delete.html'
    success_url = reverse_lazy('todo:index')

    @method_decorator(admin_or_permissions_required('pk', 'a'))
    def get(self, request, *args, **kwargs):
        return super(DeleteList, self).get(request, *args, **kwargs)


class CreateTask(LoginRequiredMixin, generic.CreateView):
    """View for creating new Task of current list."""

    form_class = AddTaskForm
    template_name = 'todo/add_task.html'
    cur_list = None

    def get_list(self):
        list_pk = self.kwargs.get('list_pk', None)
        self.cur_list = get_object_or_404(List, pk=list_pk)

    def get_context_data(self, **kwargs):
        self.get_list()
        context = super(CreateTask, self).get_context_data(**kwargs)
        context['list'] = self.cur_list
        return context

    @method_decorator(admin_or_permissions_required('list_pk', 'a', 'e'))
    def get(self, request, *args, **kwargs):
        return super(CreateTask, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        self.get_list()
        form.instance.list = self.cur_list
        form.instance.completed = False
        return super(CreateTask, self).form_valid(form)


class UpdateTask(LoginRequiredMixin, generic.UpdateView):
    """View for updating current Task."""

    form_class = UpdateTaskForm
    model = Task
    template_name = 'todo/update_task.html'

    @method_decorator(admin_or_permissions_required('list_pk', 'a', 'e'))
    def get(self, request, *args, **kwargs):
        return super(UpdateTask, self).get(request, *args, **kwargs)


class DeleteTask(LoginRequiredMixin, generic.DeleteView):
    """View for deleting current Task."""

    model = Task
    template_name = 'todo/task_confirm_delete.html'

    @method_decorator(admin_or_permissions_required('list_pk', 'a', 'e'))
    def get(self, request, *args, **kwargs):
        return super(DeleteTask, self).get(request, *args, **kwargs)

    def get_success_url(self):
        list_pk = self.kwargs.get('list_pk', None)
        self.success_url = reverse('todo:detail', kwargs={
            'pk': list_pk,
        })
        return super(DeleteTask, self).get_success_url()
