from django import forms
from .models import List, Task
from django.forms.extras.widgets import Select


class ManageListForm(forms.ModelForm):
    """Form for creating new list."""

    class Meta:
        model = List
        fields = ['name']


class AddTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ['list', 'completed']


class UpdateTaskForm(forms.ModelForm):

    class Meta:
        model = Task
        exclude = ['list']


class ShareListForm(forms.Form):

    PERMISSIONS = (
        ('r', 'Read only'),
        ('e', 'View and edit'),
    )

    username = forms.CharField(label='Username for share', max_length=100)
    permissions = forms.ChoiceField(widget=Select, choices=PERMISSIONS)
