# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='List',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('created_date', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='ListUsers',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('permission', models.CharField(choices=[('a', 'Author'), ('r', 'Read only'), ('e', 'Read and edit')], max_length=1)),
                ('list', models.ForeignKey(to='todo.List')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('description', models.TextField(null=True, blank=True)),
                ('time', models.TimeField()),
                ('completed', models.BooleanField(default=None)),
                ('list', models.ForeignKey(to='todo.List')),
            ],
        ),
        migrations.AddField(
            model_name='list',
            name='users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='todo.ListUsers'),
        ),
    ]
